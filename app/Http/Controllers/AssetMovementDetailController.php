<?php

namespace App\Http\Controllers;

use App\Models\AssetMovementDetail;
use Illuminate\Http\Request;

class AssetMovementDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(AssetMovementDetail $assetMovementDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(AssetMovementDetail $assetMovementDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, AssetMovementDetail $assetMovementDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(AssetMovementDetail $assetMovementDetail)
    {
        //
    }
}
