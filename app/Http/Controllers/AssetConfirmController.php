<?php

namespace App\Http\Controllers;

use App\Models\AssetConfirm;
use Illuminate\Http\Request;

class AssetConfirmController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(AssetConfirm $assetConfirm)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(AssetConfirm $assetConfirm)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, AssetConfirm $assetConfirm)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(AssetConfirm $assetConfirm)
    {
        //
    }
}
