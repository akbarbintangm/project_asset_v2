<?php

namespace App\Http\Controllers;

use App\Models\ItemMaster;
use Illuminate\Http\Request;

class ItemMasterController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(ItemMaster $itemMaster)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(ItemMaster $itemMaster)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, ItemMaster $itemMaster)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(ItemMaster $itemMaster)
    {
        //
    }
}
