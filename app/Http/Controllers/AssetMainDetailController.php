<?php

namespace App\Http\Controllers;

use App\Models\AssetMainDetail;
use Illuminate\Http\Request;

class AssetMainDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(AssetMainDetail $assetMainDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(AssetMainDetail $assetMainDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, AssetMainDetail $assetMainDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(AssetMainDetail $assetMainDetail)
    {
        //
    }
}
