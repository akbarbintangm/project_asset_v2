<?php

namespace App\Http\Controllers;

use App\Models\AssetConfirmDetail;
use Illuminate\Http\Request;

class AssetConfirmDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(AssetConfirmDetail $assetConfirmDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(AssetConfirmDetail $assetConfirmDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, AssetConfirmDetail $assetConfirmDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(AssetConfirmDetail $assetConfirmDetail)
    {
        //
    }
}
