<?php

namespace App\Http\Controllers;

use App\Models\AssetExpenseDetail;
use Illuminate\Http\Request;

class AssetExpenseDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(AssetExpenseDetail $assetExpenseDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(AssetExpenseDetail $assetExpenseDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, AssetExpenseDetail $assetExpenseDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(AssetExpenseDetail $assetExpenseDetail)
    {
        //
    }
}
