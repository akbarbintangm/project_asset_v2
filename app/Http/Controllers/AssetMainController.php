<?php

namespace App\Http\Controllers;

use App\Models\AssetMain;
use Illuminate\Http\Request;

class AssetMainController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(AssetMain $assetMain)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(AssetMain $assetMain)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, AssetMain $assetMain)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(AssetMain $assetMain)
    {
        //
    }
}
